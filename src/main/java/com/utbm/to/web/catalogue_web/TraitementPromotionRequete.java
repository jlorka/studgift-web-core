/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utbm.to.web.catalogue_web;

import com.utbm.to.admin.core.catalogue.core.dao.ArticleDao;
import com.utbm.to.admin.core.catalogue.core.dao.LignePromotionDao;
import com.utbm.to.admin.core.catalogue.core.dao.PromotionDao;
import com.utbm.to.admin.core.catalogue.core.entity.LignePromotion;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
//import java.text.Date;

/**
 *
 * 
 */
public class TraitementPromotionRequete extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    
    protected void processRequesUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Recuperation de l'operation
        String add=request.getParameter("add");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
         // V�rifier si c'est une requ�te d'Ajout       
        if(add.equalsIgnoreCase("add")){
            // Controler si les champs sont vides
            if((!request.getParameter("article").equalsIgnoreCase("") && request.getParameter("article")!=null)
                    && (!request.getParameter("promotion").equalsIgnoreCase("") && request.getParameter("promotion")!=null)
                    && (!request.getParameter("datedebut").equalsIgnoreCase("") && request.getParameter("datedebut")!=null)
                    && (!request.getParameter("datefin").equalsIgnoreCase("") && request.getParameter("datefin")!=null)){
                String article=request.getParameter("article");
                String promotion=request.getParameter("promotion");
                String datedebut=request.getParameter("datedebut");
                String datefin=request.getParameter("datefin");
                String valeur=request.getParameter("valeur");
                
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                 Date dated = null ;
                 Date datef= null; 
                try {

                         dated = formatter.parse(datedebut);
                         datef = formatter.parse(datefin);
                } catch (ParseException e) {
                        e.printStackTrace();
                }
                
                // Cr�ation de l'objet qui sera ins�r�
                LignePromotion lignepromo=new LignePromotion(dated, datef, null, null, Float.parseFloat(valeur));
                lignepromo.setArticle(ArticleDao.readerArticle(Long.valueOf(article)));
                lignepromo.setPromotion(PromotionDao.readerPromotion(Long.parseLong(promotion)));
                // Appel de la classe LignePromotionDao du module core pour enregistrer l'objet
                LignePromotionDao.registerLignePromotion(lignepromo);
                out.println("Enregistrer avec succ�s !");
                  RequestDispatcher dispatcher;
                  // Rediriger vers cette page
                dispatcher=request.getRequestDispatcher("form_save_promotion.jsp?save=success");
                dispatcher.forward(request, response);
            }else {
                out.println("Veuillez bien saisir les information � Enregistrer !");
                 RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_save_promotion.jsp?save=failed");
                dispatcher.forward(request, response);
            }
            // V�rifier si c'est une requ�te de modification
        }  else if(add.equalsIgnoreCase("update")){
                        // Controler si les champs sont vides
             if((!request.getParameter("article").equalsIgnoreCase("") && request.getParameter("article")!=null)
                    && (!request.getParameter("promotion").equalsIgnoreCase("") && request.getParameter("promotion")!=null)
                    && (!request.getParameter("datedebut").equalsIgnoreCase("") && request.getParameter("datedebut")!=null)
                    && (!request.getParameter("datefin").equalsIgnoreCase("") && request.getParameter("datefin")!=null)){
                String article=request.getParameter("article");
                String promotion=request.getParameter("promotion");
                String datedebut=request.getParameter("datedebut");
                String datefin=request.getParameter("datefin");
                String valeur=request.getParameter("valeur");
                // Formatage du texte en Date
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                 Date dated = null ;
                 Date datef= null; 
                try {

                         dated = formatter.parse(datedebut);
                         datef = formatter.parse(datefin);

                } catch (ParseException e) {
                        e.printStackTrace();
                }
                
                // Recuperer l'�lement � modifier avec une fonction du module core
                LignePromotion lignepromo=new LignePromotion(dated, datef, null, null, Float.parseFloat(valeur));
                lignepromo.setArticle(ArticleDao.readerArticle(Long.valueOf(article)));
                lignepromo.setPromotion(PromotionDao.readerPromotion(Long.parseLong(promotion)));
                // Enregistrer la modification
                LignePromotionDao.updateLignePromotion(lignepromo);
                out.println("Modification avec succ�s !");
                RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("liste_articles.jsp?update=success");
                dispatcher.forward(request, response);
                      
            } else {
                out.println("Veuillez bien saisir les information � Update !");
                RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("liste_articles.jsp?update=failed");
                dispatcher.forward(request, response);
            }

        }
        
        
                 
    }
    

   
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequesUpdate(request, response);
    }

   
}
