<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Reservation"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ClientDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Client"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CategorieDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Categorie"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.*"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
    String success = "";
    success = request.getParameter("save");
    Client client = new Client();
    String codeClient = "";
    if (request.getParameter("reservation") != null) {
        codeClient = request.getParameter("client");
        client = ClientDao.readerClient(codeClient);
    }
%>

<%@ include file="menu.html" %>
<h1>Liste des R&eacute;servations par Clients</h1>

<br/>
<br/><br/>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">R&eacute;servation d'Articles</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="${pageContext.request.contextPath}/liste_reservation.jsp" method="POST">
            <fieldset>
                <legend>Legend</legend>


                <div class="form-group">
                    <label for="select" class="col-lg-2 control-label">Choisir un Client :</label>
                    <div class="col-lg-10">
                        <br>
                        <select name="client"  class="form-control">
                            <%
                                for (Client cl : ClientDao.getAllClient()) {
                                    if (codeClient.equalsIgnoreCase(cl.getCodeClient())) {
                                        out.println("<option value='" + cl.getCodeClient() + "' selected >" + cl.getPrenomClient() + " " + cl.getNomClient() + " (" + cl.getCodeClient() + ")</option>");
                                    } else {
                                        out.println("<option value='" + cl.getCodeClient() + "' >" + cl.getPrenomClient() + " " + cl.getNomClient() + " (" + cl.getCodeClient() + ")</option>");
                                    }

                                }
                            %>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <!--<button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>-->
                        <button name="reservation" value="reservation" type="submit" class="btn btn-primary">Afficher les R&eacute;servations</button>
                    </div>
                </div>
            </fieldset>
        </form>



        <%  if (request.getParameter("reservation") != null) {
        %>             

        <h2>Listes des R&eacute;servations</h2>
        <table class="table table-striped table-hover ">
            <thead>
                <tr>
                    <th>No R&eacute;servation</th>
                    <th>Date</th>
                    <th>Detail</th>

                </tr>
            </thead>
            <tbody>

                <%
                    for (Reservation r : ReservationDao.getAllReservationByClient(client)) {
                        out.println("<tr>");
                        out.println("<td>" + r.getCodeReservation() + "</td>");
                        out.println("<td>" + r.getDateDeReservation() + "</td>");
                        out.println("<td><a href='detail_reservation.jsp?idreservation=" + r.getCodeReservation() + "&code=" + r.getClient().getCodeClient() + "'>Detail</a></td>");
                        out.println("</tr>");
                    }
                %>       
            </tbody>
        </table>     
        <%
      }%> 
    </div>
</div>
</body>
</html>
