<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Article"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ArticleDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CategorieDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Categorie"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%@ include file="menu.html" %>
        <h1>Gestions des articles</h1>
        <%
           String id="";
           id=request.getParameter("id");
           if(!id.equalsIgnoreCase("") && id!=null ){
             Article article=ArticleDao.readerArticle(id);
          
           
          %>
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Formulaire de saisie</h3>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" action="${pageContext.request.contextPath}/traitement" method="POST">
               <fieldset>
                 <legend>Legend</legend>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">R&eacute;f&eacute;rence</label>
                   <div class="col-lg-10">
                       <input type="text" value="<%out.println(article.getReferenceArticle());%>" class="form-control" id="reference" name="reference" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">D&eacute;signation</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" value="<%out.println(article.getDesignation());%>" id="designation" name="designation" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Marque</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="marque" value="<%out.println(article.getMarque());%>" name="marque" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Quantit&eacute;s</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="quantite" name="quantite" value="<%out.println(article.getQuantite());%>" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Prix</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="prix" value="<%out.println(article.getPrix());%>"  name="prix" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="textArea" class="col-lg-2 control-label">D&eacute;tail</label>
                   <div class="col-lg-10">
                       <textarea name="detail" class="form-control" rows="3" id="detail"><%out.println(article.getDetails());%></textarea>
                     <span class="help-block">Au moins 20 caractères.</span>
                   </div>
                 </div>
                  <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Adresse</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" value="<%out.println(article.getAdresse());%>" id="adresse" name="adresse" placeholder="">
                   </div>
                 </div>
                 
                
                 
                 <div class="form-group">
                   <label for="select" class="col-lg-2 control-label">Catalogue</label>
                   <div class="col-lg-10">
                     <br>
                     <select name="catalogue"  class="form-control">
                         <% 
                             for(Catalogue c:CatalogueDao.getAllCatalogue()){
                                if(article.getCatalogue()!=null){
                                    if(article.getCatalogue().getCodeCatalogue()==c.getCodeCatalogue()){
                                        out.println("<option value='"+c.getCodeCatalogue()+"' selected >"+c.getLibelleCatalogue()+"</option>");
                                    }
                                    else {
                                    out.println("<option value='"+c.getCodeCatalogue()+"' >"+c.getLibelleCatalogue()+"</option>");                                    
                                     }
                                }else {
                                    out.println("<option value='"+c.getCodeCatalogue()+"' >"+c.getLibelleCatalogue()+"</option>");                                    
                                }
                             }
                       %>
                     </select>
                   </div>
                 </div>
                 
                 <div class="form-group">
                     <label for="select" class="col-lg-2 control-label">Cat&eacute;gorie</label>
                   <div class="col-lg-10">
                     <br>
                     <select name="categorie"  class="form-control">
                         <%
                             for(Categorie ca:CategorieDao.getAllCategorie()){
                               if(article.getCategorie()!=null){ 
                                    if(article.getCategorie().getCodeCategorie()==ca.getCodeCategorie()){
                                        out.println("<option value='"+ca.getCodeCategorie()+"' selected >"+ca.getLibelleCategorie()+"</option>");
                                     }else {
                                        out.println("<option value='"+ca.getCodeCategorie()+"' >"+ca.getLibelleCategorie()+"</option>");                                    
                                      }
                                 }else {
                                    out.println("<option value='"+ca.getCodeCategorie()+"' >"+ca.getLibelleCategorie()+"</option>");                                    
                                }
                             }
                       %>
                     </select>
                   </div>
                 </div>
                  <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Code Postal</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="codepostal" value="<%out.println(article.getCodePostal());%>" name="codepostal" placeholder="">
                   </div>
                 </div>
               <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Ville</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="ville" value="<%out.println(article.getVille());%>" name="ville" placeholder="">
                   </div>
                 </div>
                 
                
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                       <button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>
                       <button name="add" value="update" type="submit" class="btn btn-primary">Modifier</button>
                   </div>
                 </div>
               </fieldset>
             </form>
                       <%  } %>          
  
</div>&²
</div>
  </body>
</html>
