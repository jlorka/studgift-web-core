package com.utbm.to.web.catalogue_web.sender;
import com.utbm.to.admin.core.catalogue.core.dao.AcceptationDao;
import com.utbm.to.admin.core.catalogue.core.dao.ArticleDao;
import com.utbm.to.admin.core.catalogue.core.dao.ClientDao;
import com.utbm.to.admin.core.catalogue.core.dao.ReservationDao;
import com.utbm.to.admin.core.catalogue.core.entity.Acceptation;
import com.utbm.to.admin.core.catalogue.core.entity.Article;
import com.utbm.to.admin.core.catalogue.core.entity.Client;
import com.utbm.to.admin.core.catalogue.core.entity.Reservation;

import java.sql.SQLException;

import org.json.simple.JSONObject;


public class Computation {

	// Conversion de Liste des Etudiants en Json 
	public static String articlesToJson() {
		String liste="";
		for(Article article:ArticleDao.getAllArticle()){
			if(article.getActivate()!=null && article.getActivate().equalsIgnoreCase("Y")){
			JSONObject obj = new JSONObject();
			obj.put("numero", article.getReferenceArticle());
			obj.put("name", article.getDesignation());
			obj.put("price", article.getPrix().toString());
			if(article.getImage()!=null){
				byte[] bdata = null;
				try {
					bdata = article.getImage().getBytes(1, (int) article.getImage().length());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String text = new String(bdata);
				obj.put("image", text);
			}

			if(article.getClient()!=null) obj.put("user", article.getClient().getCodeClient());
			if(article.getClient()!=null) obj.put("useremail", article.getClient().getMail());
                        obj.put("marque", article.getMarque());
			obj.put("description", article.getDetails());
			obj.put("ville", article.getVille());
			if(liste.equals("")) liste+=obj.toJSONString(); else liste+=";"+obj.toJSONString();
		}
		}
		return liste;
	}
	
	
	
	// Conversion de Liste des Etudiants en Json 
		public static String  postulantsToJsonParAticle(Long articleRef) {
			String liste="";
			for(Reservation reservation:ReservationDao.getAllReservationByArtcle(ArticleDao.readerArticle(articleRef))){
				JSONObject obj = new JSONObject();
				obj.put("numero", reservation.getCodeReservation());
				obj.put("article", reservation.getArticle().getReferenceArticle());
				obj.put("idclient", reservation.getClient().getCodeClient());
				obj.put("loginclient", reservation.getClient().getMail());
				obj.put("nomclient", reservation.getClient().getNomClient());
				obj.put("prenomclient", reservation.getClient().getPrenomClient());
				//obj.put("date", reservation.getDateDeReservation());
				if(liste.equals("")) liste+=obj.toJSONString(); else liste+=";"+obj.toJSONString();
			}
			return liste;
		}
		
		// Conversion de Liste des Etudiants en Json 
				public static String  acceptationDons(Long client) {
					String liste="";
					for(Acceptation acceptation:AcceptationDao.getAllAcceptationByClient(ClientDao.readerClient(client))){
						JSONObject obj = new JSONObject();
						obj.put("numero", acceptation.getCodeAcceptation());
						obj.put("article", acceptation.getArticle().getReferenceArticle());
						obj.put("nomarticle", acceptation.getArticle().getDesignation());
						obj.put("idclient", acceptation.getArticle().getClient().getCodeClient());
						obj.put("loginclient", acceptation.getArticle().getClient().getMail());
						obj.put("nomclient", acceptation.getArticle().getClient().getNomClient());
						obj.put("prenomclient", acceptation.getArticle().getClient().getPrenomClient());
						
						
						if(liste.equals("")) liste+=obj.toJSONString(); else liste+=";"+obj.toJSONString();
					}
					return liste;
				}
	
	
	public static Client getClientByEmail(String email) {
		Client cli=null;
		for(Client client:ClientDao.getAllClient()){
			if(client.getMail()!=null && client.getMail().equals(email)) {
				cli=client;
				break;
			}
		}
		return cli;
	}

	
	

	public static void main(String[] args) {
		System.out.println(""+articlesToJson());

	}



}
