<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ClientDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Client"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ReservationDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.LigneReservationDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.LigneReservation"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ClientDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Client"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%  String codeClient="";
            String chaine="";
            Client cl=new Client();
            if(request.getParameter("code")!=null){
                codeClient=request.getParameter("code");
                cl=ClientDao.readerClient(codeClient);
                out.print("");
                chaine=" >"+cl.getPrenomClient()+" "+cl.getNomClient()+" ("+cl.getCodeClient()+")";
            }
           %>

           <%@ include file="menu.html" %>
           <h1>Gestions des Clients - Client d&eacute;tail </h1>
        
        <br/>
        <br/><br/>
        <div class="panel panel-primcly">
  <div class="panel-heading">
      <h3 class="panel-title"> Client No <% out.print(chaine); %></h3>
  </div>
  <div class="panel-body">
      
            
  <br/>
   <br/>
   <h2>Detail des information du Client</h2>
        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>Rubrique</th>
                <th>Valeurs</th>
              </tr>
            </thead>
            <tbody>
                
              <%
                 if(request.getParameter("code")!=null){ 
                  out.println("<tr>");
                       out.println("<td>Code</td>");  out.println("<td>"+cl.getCodeClient()+"</td>");
                  out.println("</tr>");
                 out.println("<tr>");
                       out.println("<td>Nom</td>");  out.println("<td>"+cl.getNomClient()+"</td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Prenom</td>");  out.println("<td>"+cl.getPrenomClient()+"</td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Date Naissance</td>");  out.println("<td>"+cl.getDateNais()+"</td>"); 
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Email</td>");  out.println("<td>"+cl.getMail()+"</td>"); 
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Telephone</td>");  out.println("<td>"+cl.getTelephone()+"</td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Ville</td>");  out.println("<td>"+cl.getVille()+"</td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Rue</td>");  out.println("<td><p>"+cl.getRue()+"</p></td>");
                  out.println("</tr>");
                                 
                 }
              %>
            </tbody>
        </table>
</div>
</div>
  </body>
</html>
