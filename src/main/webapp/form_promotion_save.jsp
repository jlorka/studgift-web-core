<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ArticleDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Article"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.PromotionDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Promotion"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CategorieDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Categorie"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%
           String success="";
           success=request.getParameter("save");
           %>


        <%@ include file="menu.html" %>
        <h1>Gestions des promotions</h1>
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Formulaire de saisie</h3>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" action="${pageContext.request.contextPath}/traitement" method="POST">
               <fieldset>
                 <legend>Legend</legend>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Promotion</label>
                   <div class="col-lg-10">
                     <select name="promotion"  class="form-control">
                         <%
                             for(Promotion promotion:PromotionDao.getAllPromotion()){
                                out.println("<option value='"+promotion.getCodePromotion()+"' >"+promotion.getLibellePromotion()+"</option>");
                             }
                       %>
                     </select>
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Article</label>
                   <div class="col-lg-10">
                     <select name="article"  class="form-control">
                         <%
                             for(Article article:ArticleDao.getAllArticle()){
                                out.println("<option value='"+article.getReferenceArticle()+"' >"+article.getDesignation()+"</option>");
                             }
                       %>
                     </select>
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Date debut:</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="datepicker" name="datedebut" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Date Fin</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="datepicker1" name="datefin" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Valeur promotion</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="valeur" name="valeur" placeholder="">
                   </div>
                 </div>
                
                
                   
                
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                       <button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>
                       <button name="add" value="add" type="submit" class="btn btn-primary">Enregistrer</button>
                   </div>
                 </div>
                     <%
                         if(success!=null){    
                            if(success.equalsIgnoreCase("success")){
                                out.println("<p style='color: #9C6' >Enregistrement avec succès !</p>");
                             } else if(success.equalsIgnoreCase("failed")){
                                out.println("<p style='color: #FF0000' >Erreur lors de l'enregistrement !</p>");
                             }
                         }
                        %>
               </fieldset>
             </form>
            
  
</div>
</div>
  </body>
</html>
