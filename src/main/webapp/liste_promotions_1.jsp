<%@page import="com.utbm.to.admin.core.catalogue.core.entity.*"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


        <%
                   String success="";
                   success=request.getParameter("update");

            %>

        <%@ include file="menu.html" %>
        <h1>Gestions des promotions</h1>
        <a href="#" class="btn btn-default btn-lg btn-block">La liste des Promotions</a>
        <br/>
        <br/>
        
            <%
               if(success!=null){     
                    if(!success.equalsIgnoreCase("") && success!=null){
                     out.println("<p style='color: #9C6' >Modifier avec succès !</p>");
                   }
               }
            %>  
            
        <br/>
        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>Promotion</th>
                <th>Article</th>
                <th>Date Debut</th>
                <th>Date Fin</th>
                <th>Valeur</th>
                <th>Detail</th>
                <th>Modifier</th>
                <th>Supprimer</th>
              </tr>
            </thead>
            <tbody>
                
              <%
                  
                  for(LignePromotion a:LignePromotionDao.getAllLignePromotion()){
                  out.println("<tr>");
                                    out.println("<td>"+a.getPromotion().getLibellePromotion()+"</td>");
                                    out.println("<td>"+a.getArticle().getDesignation()+"</td>");
                                    out.println("<td>"+a.getDebutPromotion()+"</td>");
                                    out.println("<td>"+a.getFinPromotion()+"</td>");
                                    out.println("<td>"+a.getValeurPromotion()+"</td>");
                                    out.println("<td><a href='detail_article.jsp?id="+a.getPromotion().getCodePromotion()+"&ida="+a.getArticle().getReferenceArticle()+"'>Detail..</a></td>");
                                    out.println("<td><a href='form_update.jsp?id="+a.getPromotion().getCodePromotion()+"&ida="+a.getArticle().getReferenceArticle()+"'>U</a></td>");
                                    out.println("<td><a href='delete.jsp?id="+a.getPromotion().getCodePromotion()+"&ida="+a.getArticle().getReferenceArticle()+"'>D</a></td>");
                   out.println("</tr>");
                  }
              %>
            </tbody>
          </table> 
    </body>
</html>
