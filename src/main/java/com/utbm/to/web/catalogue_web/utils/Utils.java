package com.utbm.to.web.catalogue_web.utils;



import biz.source_code.base64Coder.Base64Coder;

public class Utils {

	public static byte[] Base64ToBytes(String imageString)  {
		byte[] decodedBytes = Base64Coder.decode(imageString);
		return decodedBytes;
	}
	
	
}
