<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ClientDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Client"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ReservationDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.LigneReservationDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.LigneReservation"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ArticleDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Article"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%
           String success="";
           String codeClient="";
           String codeReservation="";
           codeClient=request.getParameter("code");
           codeReservation=request.getParameter("idreservation");
           success=request.getParameter("save");
           Client cl=ClientDao.readerClient(codeClient);
           String chaineClient=cl.getCodeClient()+"' >"+cl.getPrenomClient()+" "+cl.getNomClient()+" ("+cl.getCodeClient()+")";
           %>

           <%@ include file="menu.html" %>
        <h1>Gestions des Reservations - Ajout Article(s)</h1>
        
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
      <h3 class="panel-title">Ajout Articles pour Reservation No <%out.print(codeReservation);%> par <% out.print(chaineClient); %></h3>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" action="${pageContext.request.contextPath}/traitementaddarticlereservation" method="POST">
               <fieldset>
                 <legend>Legend</legend>
                 
                  <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Article</label>
                   <div class="col-lg-10">
                     <select name="article"  class="form-control">
                         <%
                             for(Article article:ArticleDao.getAllArticle()){
                                out.println("<option value='"+article.getReferenceArticle()+"' >"+article.getDesignation()+"</option>");
                             }
                       %>
                     </select>
                   </div>
                 </div>
                 
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Quantit&eacute; :</label>
                   <div class="col-lg-10">
                       <input type="hidden" class="form-control" id="client" name="client" value="<%out.print(codeClient);%>" >
                       <input type="hidden" class="form-control" id="reservation" name="reservation" value="<%out.print(codeReservation);%>">
                      <input type="text" class="form-control" id="quantite" name="quantite" placeholder="">
                   </div>
                 </div>
                 
                 
                 <!--<div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Date de Cr&eacute;ation :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="marque" name="datecreation" placeholder="">
                   </div>
                 </div> -->
                 
                   <%
                         if(success!=null){    
                            if(success.equalsIgnoreCase("success")){
                                out.println("<p style='color: #9C6' >Enregistrement avec succès !</p>");
                             } else if(success.equalsIgnoreCase("failed")){
                                out.println("<p style='color: #FF0000' >Erreur lors de l'enregistrement !</p>");
                             }
                         }
                        %>
                
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                       <button name="add" value="add" type="submit" class="btn btn-primary">Ajouter Article</button>
                   </div>
                 </div>
               </fieldset>
             </form>
            
  <br/>
   <br/>
   <h2>Listes des Articles R&eacute;serv&eacute;s</h2>
        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>No R&eacute;serv&eacute;s</th>
                <th>Article</th>
                <th>quantit&eacute;(s)</th>
                <th>Supprimer</th>
              </tr>
            </thead>
            <tbody>
                
              <%
                  
                  for(LigneReservation lr:LigneReservationDao.getAllLigneReservationByReservation(ReservationDao.readerReservation(Long.parseLong(codeReservation)))){
                  out.println("<tr>");
                                    out.println("<td>"+lr.getReservation().getCodeReservation()+"</td>");
                                    out.println("<td>"+lr.getArticle().getDesignation()+"</td>");
                                    out.println("<td>"+lr.getQuantiteReservee()+"</td>");
                                    out.println("<td><a href='form_catalogue_update.jsp?id="+lr.getReservation().getCodeReservation()+"'>U</a></td>");
                                    out.println("<td><a href='delete.jsp?id="+lr.getReservation().getCodeReservation()+"'>D</a></td>");
                   out.println("</tr>");
                  }
              %>
              </tbody>
        </table> 
             
</div>
</div>
  </body>
</html>
