/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utbm.to.web.catalogue_web;


import com.utbm.to.admin.core.catalogue.core.dao.ArticleDao;
import com.utbm.to.admin.core.catalogue.core.dao.ClientDao;
import com.utbm.to.admin.core.catalogue.core.dao.ReservationDao;
import com.utbm.to.admin.core.catalogue.core.entity.Reservation;
import com.utbm.to.web.catalogue_web.sender.Computation;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 
 */
public class TraitementReservationRequete extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    
     protected void processRequesUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String reservation=request.getParameter("reservation");
         response.setContentType("application/json");
        //Formatage
    	response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
                 // V�rifier si c'est une requ�te de reservation      
        if(reservation.equalsIgnoreCase("reservation")){
           // Controler si les champs sont vides
            if((!request.getParameter("client").equalsIgnoreCase("") && request.getParameter("client")!=null 
            		&& !request.getParameter("article").equalsIgnoreCase("") && request.getParameter("article")!=null)){
                String client=request.getParameter("client");
                String article=request.getParameter("article");
                // Cr�ation de l'objet qui sera ins�r�
                Reservation reservationClient=new Reservation();
                reservationClient.setClient(Computation.getClientByEmail(client));
                reservationClient.setArticle(ArticleDao.readerArticle(Long.valueOf(article)));
                reservationClient.setDateDeReservation(new Date());
                // Appel de la classe ReservationDao du module core pour enregistrer l'objet
                ReservationDao.registerReservation(reservationClient);
               // Rediriger vers cette page
                out.println("OK");
                /*RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_reservation_addarticle.jsp?code="+client+"&idreservation="+reservationClient.getCodeReservation());
                dispatcher.forward(request, response);*/
            }else {
                out.println("ERROR");
                /* RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_save.jsp?save=failed");
                dispatcher.forward(request, response);*/
            }

        }  
        
                 
    }
    
    

  
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequesUpdate(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequesUpdate(request, response);
    }

}
