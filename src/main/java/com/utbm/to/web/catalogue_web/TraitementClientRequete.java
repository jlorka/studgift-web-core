/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utbm.to.web.catalogue_web;

import com.utbm.to.admin.core.catalogue.core.dao.ArticleDao;
import com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao;
import com.utbm.to.admin.core.catalogue.core.dao.CategorieDao;
import com.utbm.to.admin.core.catalogue.core.dao.ClientDao;
import com.utbm.to.admin.core.catalogue.core.entity.Article;
import com.utbm.to.admin.core.catalogue.core.entity.Client;
import com.utbm.to.web.catalogue_web.service.Operations;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 
 */
public class TraitementClientRequete extends HttpServlet {

    /**
     * Processes requests for both HTTP  <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequesUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String add=request.getParameter("add");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
         // V�rifier si c'est une requ�te d'Ajout 
        
        if(request.getParameter("target").equalsIgnoreCase("ANDROID")){
            
            if(add.equalsIgnoreCase("add")){
            // Controler si les champs sont vides
            if((!request.getParameter("nom").equalsIgnoreCase("") && request.getParameter("nom")!=null)
                    && (!request.getParameter("prenom").equalsIgnoreCase("") && request.getParameter("prenom")!=null)){
                
                String nom=request.getParameter("nom");
                String prenom=request.getParameter("prenom");
                String Rue=request.getParameter("rue");        
                String mpd=request.getParameter("mpd");
                String datenais=request.getParameter("datenais");
                String email=request.getParameter("email");
                String codepostal=request.getParameter("codepostal");
                String ville=request.getParameter("ville");
                String telephone=request.getParameter("telephone");
                String identifiant=request.getParameter("email");
                // Formatage en type Date
                 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                 Date daten= null; 
                try {
                         daten = formatter.parse(datenais);
                     
                } catch (ParseException e) {
                        e.printStackTrace();
                }
                if(Operations.userExist(email)){
                out.println("EXIST");
                }
                else {
                // Cr�ation de l'objet qui sera ins�r�               
                Client client=new Client(null, nom, prenom, Rue, codepostal, ville, telephone, email, identifiant, mpd ,new Date());
                // Appel de la classe CatalogueDao du module core pour enregistrer l'objet
                ClientDao.registerClient(client);
                out.println("OK");
                }
            }else {
                out.println("NOTENOUGTH");
            }
             // V�rifier si c'est une requ�te de modification
        }  else if(add.equalsIgnoreCase("update")){
            // Controler si les champs sont vides
            if((!request.getParameter("code").equalsIgnoreCase("") && request.getParameter("code")!=null)
                    && (!request.getParameter("nom").equalsIgnoreCase("") && request.getParameter("nom")!=null)
                    && (!request.getParameter("prenom").equalsIgnoreCase("") && request.getParameter("prenom")!=null)
                    && (!request.getParameter("Rue").equalsIgnoreCase("") && request.getParameter("Rue")!=null)){
                String code=request.getParameter("code");
                String nom=request.getParameter("nom");
                String prenom=request.getParameter("prenom");
                String Rue=request.getParameter("rue");
                String mpd=request.getParameter("mpd");
                String datenais=request.getParameter("datenais");
                String email=request.getParameter("email");
                String codepostal=request.getParameter("codepostal");
                String ville=request.getParameter("ville");
                String telephone=request.getParameter("telephone");
                String identifiant=request.getParameter("email");
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                 Date daten= null; 
                try {
                         daten = formatter.parse(datenais);
                     
                } catch (ParseException e) {
                        e.printStackTrace();
                }
                // Cr�ation de l'objet � modifier
                Client client=new Client(null, nom, prenom, Rue, codepostal, ville, telephone, email, identifiant, mpd ,daten,  null);
                // Appel de la classe ClientDao du module core pour modifier dansla base
                ClientDao.updateClient(client);
                out.println("Modification avec succ�s !");
                RequestDispatcher dispatcher;
                // Rediriger vers cette page
                dispatcher=request.getRequestDispatcher("liste_articles.jsp?update=success");
                dispatcher.forward(request, response);
                      
            } else {
                out.println("Veuillez bien saisir les information � Update !");
                RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("liste_articles.jsp?update=failed");
                dispatcher.forward(request, response);
            }

        }
            
        } else {
        
        if(add.equalsIgnoreCase("add")){
            // Controler si les champs sont vides
            if((!request.getParameter("nom").equalsIgnoreCase("") && request.getParameter("nom")!=null)
                    && (!request.getParameter("prenom").equalsIgnoreCase("") && request.getParameter("prenom")!=null)
                    && (!request.getParameter("rue").equalsIgnoreCase("") && request.getParameter("rue")!=null)){
                
                String nom=request.getParameter("nom");
                String prenom=request.getParameter("prenom");
                String Rue=request.getParameter("rue");        
                String mpd=request.getParameter("mpd");
                String datenais=request.getParameter("datenais");
                String email=request.getParameter("email");
                String codepostal=request.getParameter("codepostal");
                String ville=request.getParameter("ville");
                String telephone=request.getParameter("telephone");
                String identifiant=request.getParameter("email");
                // Formatage en type Date
                 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                 Date daten= null; 
                try {
                         daten = formatter.parse(datenais);
                     
                } catch (ParseException e) {
                        e.printStackTrace();
                }
                
                // Cr�ation de l'objet qui sera ins�r�               
                Client client=new Client(null, nom, prenom, Rue, codepostal, ville, telephone, email, identifiant, mpd ,daten,  null);
                // Appel de la classe CatalogueDao du module core pour enregistrer l'objet
                ClientDao.registerClient(client);
                out.println("Enregistrer avec succ�s !");
                 RequestDispatcher dispatcher;
                 // Rediriger vers cette page
                dispatcher=request.getRequestDispatcher("form_save.jsp?save=success");
                dispatcher.forward(request, response);
            }else {
                out.println("Veuillez bien saisir les information � Enregistrer !");
                 RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_save.jsp?save=failed");
                dispatcher.forward(request, response);
            }
             // V�rifier si c'est une requ�te de modification
        }  else if(add.equalsIgnoreCase("update")){
            // Controler si les champs sont vides
            if((!request.getParameter("code").equalsIgnoreCase("") && request.getParameter("code")!=null)
                    && (!request.getParameter("nom").equalsIgnoreCase("") && request.getParameter("nom")!=null)
                    && (!request.getParameter("prenom").equalsIgnoreCase("") && request.getParameter("prenom")!=null)
                    && (!request.getParameter("Rue").equalsIgnoreCase("") && request.getParameter("Rue")!=null)){
                String code=request.getParameter("code");
                String nom=request.getParameter("nom");
                String prenom=request.getParameter("prenom");
                String Rue=request.getParameter("rue");
                String mpd=request.getParameter("mpd");
                String datenais=request.getParameter("datenais");
                String email=request.getParameter("email");
                String codepostal=request.getParameter("codepostal");
                String ville=request.getParameter("ville");
                String telephone=request.getParameter("telephone");
                String identifiant=request.getParameter("email");
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                 Date daten= null; 
                try {
                         daten = formatter.parse(datenais);
                     
                } catch (ParseException e) {
                        e.printStackTrace();
                }
                // Cr�ation de l'objet � modifier
                Client client=new Client(null, nom, prenom, Rue, codepostal, ville, telephone, email, identifiant, mpd ,daten,  null);
                // Appel de la classe ClientDao du module core pour modifier dansla base
                ClientDao.updateClient(client);
                out.println("Modification avec succ�s !");
                RequestDispatcher dispatcher;
                // Rediriger vers cette page
                dispatcher=request.getRequestDispatcher("liste_articles.jsp?update=success");
                dispatcher.forward(request, response);
                      
            } else {
                out.println("Veuillez bien saisir les information � Update !");
                RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("liste_articles.jsp?update=failed");
                dispatcher.forward(request, response);
            }

        }
        
        }
                 
    }
    
   
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequesUpdate(request, response);
    }

   @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequesUpdate(request, response);
    }

}
