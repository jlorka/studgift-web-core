<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CategorieDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Categorie"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%
           String success="";
           success=request.getParameter("save");
           %>

           <%@ include file="menu.html" %>
        <h1>Gestions des articles - Categorie</h1>
        
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
      <h3 class="panel-title">Formulaire des Cat&eacute;gorie</h3>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" action="${pageContext.request.contextPath}/traitementcategorie" method="POST">
               <fieldset>
                 <legend>Legend</legend>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Libell&eacute;</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="reference" name="libelle" placeholder="">
                   </div>
                 </div>
                 
                   <%
                         if(success!=null){    
                            if(success.equalsIgnoreCase("success")){
                                out.println("<p style='color: #9C6' >Enregistrement avec succès !</p>");
                             } else if(success.equalsIgnoreCase("failed")){
                                out.println("<p style='color: #FF0000' >Erreur lors de l'enregistrement !</p>");
                             }
                         }
                        %>
                
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                       <button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>
                       <button name="add" value="add" type="submit" class="btn btn-primary">Enregistrer</button>
                   </div>
                 </div>
               </fieldset>
             </form>
            
  <br/>
   <br/>
   <h1>Listes des cat&eacute;gories</h1>
        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>No Categorie</th>
                <th>Libelle</th>
                <th>Supprimer</th>
              </tr>
            </thead>
            <tbody>
                
              <%
                  
                  for(Categorie a:CategorieDao.getAllCategorie()){
                  out.println("<tr>");
                                    out.println("<td>"+a.getCodeCategorie()+"</td>");
                                    out.println("<td>"+a.getLibelleCategorie()+"</td>");
                                    out.println("<td><a href='delete.jsp?id="+a.getCodeCategorie()+"'>D</a></td>");
                   out.println("</tr>");
                  }
              %>
</div>
</div>
  </body>
</html>
