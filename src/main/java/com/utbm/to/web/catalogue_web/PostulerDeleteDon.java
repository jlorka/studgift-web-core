package com.utbm.to.web.catalogue_web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utbm.to.admin.core.catalogue.core.dao.ArticleDao;
import com.utbm.to.admin.core.catalogue.core.dao.ReservationDao;
import com.utbm.to.admin.core.catalogue.core.entity.Article;

/**
 * Servlet implementation class PostulerDeleteDon
 */
public class PostulerDeleteDon extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	/**
     * @see HttpServlet#HttpServlet()
     */
    public PostulerDeleteDon() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    protected void processRequesUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String delete=request.getParameter("delete");
        response.setContentType("application/json");
        //Formatage
    	response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
                 // V�rifier si c'est une requ�te de reservation      
        if(delete.equalsIgnoreCase("delete")){
           // Controler si les champs sont vides
            if(( !request.getParameter("article").equalsIgnoreCase("") && request.getParameter("article")!=null)){
               
                String articleRef=request.getParameter("article");
                // Cr�ation de l'objet qui sera ins�r�
                Article article=ArticleDao.readerArticle(Long.valueOf(articleRef));
                article.setActivate("N");
             // Enregistrer la modification
                ArticleDao.updateArticle(article);
               // Rediriger vers cette page
                out.println("OK");
                /*RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_reservation_addarticle.jsp?code="+client+"&idreservation="+reservationClient.getCodeReservation());
                dispatcher.forward(request, response);*/
            }else {
                out.println("ERROR");
                /* RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_save.jsp?save=failed");
                dispatcher.forward(request, response);*/
            }

        }  
        
                 
    }
    
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
            processRequesUpdate(request,response);
        }

	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
                processRequesUpdate(request,response);
        }

}
