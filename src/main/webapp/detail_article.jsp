<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ClientDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Client"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ReservationDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.LigneReservationDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.LigneReservation"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ArticleDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Article"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%  String codeArticle="";
            String chaine="";
            Article ar=new Article();
            if(request.getParameter("code")!=null){
                codeArticle=request.getParameter("code");
                ar=ArticleDao.readerArticle(codeArticle);
                out.print("");
                chaine=" >"+ar.getDesignation()+"  ("+ar.getReferenceArticle()+")";
            }
           %>

           <%@ include file="menu.html" %>
           <h1>Gestions des Articles - Article d&eacute;tail </h1>
        
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
      <h3 class="panel-title"> Article No <% out.print(chaine); %></h3>
  </div>
  <div class="panel-body">
      
            
  <br/>
   <br/>
   <h2>Detail des information de l'article</h2>
        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>Rubrique</th>
                <th>Valeurs</th>
              </tr>
            </thead>
            <tbody>
                
              <%
                 if(request.getParameter("code")!=null){ 
                  out.println("<tr>");
                       out.println("<td>Reference</td>");  out.println("<td>"+ar.getReferenceArticle()+"</td>");
                  out.println("</tr>");
                 out.println("<tr>");
                       out.println("<td>Designation</td>");  out.println("<td>"+ar.getDesignation()+"</td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Marque</td>");  out.println("<td>"+ar.getMarque()+"</td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Catalogue</td>");  if(ar.getCatalogue()!=null)out.println("<td>"+ar.getCatalogue().getLibelleCatalogue()+"</td>"); else out.println("<td></td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Categorie</td>");  if(ar.getCategorie()!=null) out.println("<td>"+ar.getCategorie().getLibelleCategorie()+"</td>");else out.println("<td></td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Prix</td>");  out.println("<td>"+ar.getPrix()+"</td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Quantite(s)</td>");  out.println("<td>"+ar.getQuantite()+"</td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Description</td>");  out.println("<td><p>"+ar.getDetails()+"</p></td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Ville</td>");  out.println("<td>"+ar.getVille()+"</td>");
                  out.println("</tr>");
                  out.println("<tr>");
                       out.println("<td>Adresse</td>");  out.println("<td>"+ar.getAdresse()+"</td>");
                  out.println("</tr>");
                 
                 }
              %>
            </tbody>
        </table>
</div>
</div>
  </body>
</html>
