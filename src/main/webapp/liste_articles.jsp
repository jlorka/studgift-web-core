
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.*"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


        <%
                   String success="";
                   success=request.getParameter("update");
                   Article article = new Article();
                    String codeArticle = "";
                    if (request.getParameter("reservation") != null) {
                        codeArticle = request.getParameter("article");
                        article = ArticleDao.readerArticle(codeArticle);
                    }

            %>

            <%@ include file="menu.html" %>
        <h1>Gestions des articles</h1>
        <a href="#" class="btn btn-default btn-lg btn-block">La liste des Articles</a>
        <br/>
        <br/>
        
        
        <form class="form-horizontal" action="${pageContext.request.contextPath}/liste_articles.jsp" method="POST">
            <fieldset>
                <legend>Rechercher un Article par :</legend>


                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Reference</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="reference" name="article" placeholder="" value="<%out.print(codeArticle);%>" >
                   </div>
                 </div>
                 <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <!--<button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>-->
                        <button name="reservation" value="reservation" type="submit" class="btn btn-primary">Rechercher</button>
                    </div>
                </div>
            </fieldset>
        </form>
        
        
            <%
               if(success!=null){     
                    if(!success.equalsIgnoreCase("") && success!=null){
                     out.println("<p style='color: #9C6' >Modifier avec succès !</p>");
                   }
               }
            %>  
            
        <br/>
        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>Numéro</th>
                <th>Marque</th>
                <th>Désignation</th>
                <th>nombre d'Article</th>
                <th>Detail</th>
                <th>Modifier</th>
                <th>Supprimer</th>
              </tr>
            </thead>
            <tbody>
                
              <%
               if (request.getParameter("reservation") == null || codeArticle=="" || codeArticle==null ) {     
                  for(Article a:ArticleDao.getAllArticle()){
                  out.println("<tr>");
                                    out.println("<td>"+a.getReferenceArticle()+"</td>");
                                    out.println("<td>"+a.getMarque()+"</td>");
                                    out.println("<td>"+a.getDesignation()+"</td>");
                                    out.println("<td>"+a.getQuantite()+"</td>");
                                    out.println("<td><a href='detail_article.jsp?code="+a.getReferenceArticle()+"'>Detail..</a></td>");
                                    out.println("<td><a href='form_update.jsp?id="+a.getReferenceArticle()+"'>U</a></td>");
                                    out.println("<td><a href='delete.jsp?id="+a.getReferenceArticle()+"'>D</a></td>");
                   out.println("</tr>");
                  }
               }else {
                    if(article!=null){ 
                        out.println("<tr>");
                                    out.println("<td>"+article.getReferenceArticle()+"</td>");
                                    out.println("<td>"+article.getMarque()+"</td>");
                                    out.println("<td>"+article.getDesignation()+"</td>");
                                    out.println("<td>"+article.getQuantite()+"</td>");
                                    out.println("<td><a href='detail_article.jsp?code="+article.getReferenceArticle()+"'>Detail..</a></td>");
                                    out.println("<td><a href='form_update.jsp?id="+article.getReferenceArticle()+"'>U</a></td>");
                                    out.println("<td><a href='delete.jsp?id="+article.getReferenceArticle()+"'>D</a></td>");
                   out.println("</tr>");
                        
                    }
                }
              %>
            </tbody>
          </table> 
    </body>
</html>
