<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ClientDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Client"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ReservationDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.LigneReservationDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.LigneReservation"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ArticleDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Article"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%
           String success="";
           String codeClient="";
           String codeReservation="";
           codeClient=request.getParameter("code");
           codeReservation=request.getParameter("idreservation");
           success=request.getParameter("save");
           Client cl=ClientDao.readerClient(codeClient);
           String chaineClient=" >"+cl.getPrenomClient()+" "+cl.getNomClient()+" ("+cl.getCodeClient()+")";
          %>

           <%@ include file="menu.html" %>
           <h1>Gestions des Reservations - Article(s) R&eacute;serv&eacute;(s)</h1>
        
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
      <h3 class="panel-title"> Reservation No <%out.print(codeReservation);%> par <% out.print(chaineClient); %></h3>
  </div>
  <div class="panel-body">
      
            
  <br/>
   <br/>
   <h2>Listes des Articles R&eacute;serv&eacute;s</h2>
        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>No R&eacute;serv&eacute;s</th>
                <th>Article</th>
                <th>quantit&eacute;(s)</th>
                <th>Supprimer</th>
              </tr>
            </thead>
            <tbody>
                
              <%
                  
                  for(LigneReservation lr:LigneReservationDao.getAllLigneReservationByReservation(ReservationDao.readerReservation(Long.parseLong(codeReservation)))){
                  out.println("<tr>");
                                    out.println("<td>"+lr.getReservation().getCodeReservation()+"</td>");
                                    out.println("<td>"+lr.getArticle().getDesignation()+"</td>");
                                    out.println("<td>"+lr.getQuantiteReservee()+"</td>");
                                    out.println("<td><a href='delete.jsp?id="+lr.getReservation().getCodeReservation()+"'>D</a></td>");
                   out.println("</tr>");
                  }
              %>
             </tbody>
        </table>
</div>
</div>
  </body>
</html>
