/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utbm.to.web.catalogue_web.sender;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 
 */
public class CatalogueServer extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     * @param request servlet request
     * @param response servlet response
     * 
     */
    //Sending Article Liste Request 
    public void sendArticlesProcess(HttpServletRequest request, HttpServletResponse response) throws IOException{
    	PrintWriter out =response.getWriter();
    	response.setContentType("application/json");
        //Formatage
    	response.setCharacterEncoding("UTF-8");
    	String addressMac = request.getParameter("mac");
    	String operation = request.getParameter("operation");
        /// Envoie de liste de Articles sous format Json
    	
        if(operation!=null)
        	if(operation.equalsIgnoreCase("ARTICLES")){
                out.println(Computation.articlesToJson());}
        else if(operation.equalsIgnoreCase("POSTULANTS")){
        	String article = request.getParameter("article");
        	 out.println(Computation.postulantsToJsonParAticle(Long.valueOf(article)));
        }else if(operation.equalsIgnoreCase("ACCEPTATIONS")){
        	String client = request.getParameter("client");
    	 	out.println(Computation.acceptationDons(Long.valueOf(client)));
        }
    }
           

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               // processRequest(request, response);

            sendArticlesProcess(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        sendArticlesProcess(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
