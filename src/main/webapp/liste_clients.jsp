
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.*"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


        <%
                String success="";
                Client cl=null;
                success=request.getParameter("update");
                String codeClient="";
                if(request.getParameter("reservation")!=null ){
                codeClient=request.getParameter("client");
                cl=ClientDao.readerClient(codeClient);
           }
           %>

            <%@ include file="menu.html" %>
        <h1>Gestions des articles</h1>
        <a href="#" class="btn btn-default btn-lg btn-block">La liste des Clients</a>
        <br/>
        <br/>
        
         <form class="form-horizontal" action="${pageContext.request.contextPath}/liste_clients.jsp" method="POST">
            <fieldset>
                <legend>Rechercher un client par :</legend>


                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Code</label>
                   <div class="col-lg-10">
                       <input type="text" class="form-control" id="reference" name="client" placeholder="" value="<%out.print(codeClient);%>">
                   </div>
                 </div>
                 <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <!--<button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>-->
                        <button name="reservation" value="reservation" type="submit" class="btn btn-primary">Rechercher</button>
                    </div>
                </div>
            </fieldset>
        </form>
        
        
        
        <br/>
        
            <%
               if(success!=null){     
                    if(!success.equalsIgnoreCase("") && success!=null){
                     out.println("<p style='color: #9C6' >Modifier avec succès !</p>");
                   }
               }
            %>  
            
        <br/>
        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>Code</th>
                <th>Nom</th>
                <th>Pr&eacute;nom</th>
                <th>Email</th>
                <th>Detail</th>
                <th>Modifier</th>
                <th>Supprimer</th>
              </tr>
            </thead>
            <tbody>
                
              <%
                if (request.getParameter("reservation") == null || codeClient=="" || codeClient==null ) {  
                  for(Client c:ClientDao.getAllClient()){
                  out.println("<tr>");
                                    out.println("<td>"+c.getCodeClient()+"</td>");
                                    out.println("<td>"+c.getNomClient()+"</td>");
                                    out.println("<td>"+c.getPrenomClient()+"</td>");
                                    out.println("<td>"+c.getMail()+"</td>");
                                    out.println("<td><a href='detail_client.jsp?code="+c.getCodeClient()+"'>Detail..</a></td>");
                                    out.println("<td><a href='form_client_update.jsp?id="+c.getCodeClient()+"'>U</a></td>");
                                    out.println("<td><a href='delete_client.jsp?id="+c.getCodeClient()+"'>D</a></td>");
                   out.println("</tr>");
                  }
                }
                else {
                    if(cl!=null){ 
                        out.println("<tr>");
                                        out.println("<td>"+cl.getCodeClient()+"</td>");
                                        out.println("<td>"+cl.getNomClient()+"</td>");
                                        out.println("<td>"+cl.getPrenomClient()+"</td>");
                                        out.println("<td>"+cl.getMail()+"</td>");
                                        out.println("<td><a href='detail_client.jsp?code="+cl.getCodeClient()+"'>Detail..</a></td>");
                                        out.println("<td><a href='form_client_update.jsp?id="+cl.getCodeClient()+"'>U</a></td>");
                                        out.println("<td><a href='delete_client.jsp?id="+cl.getCodeClient()+"'>D</a></td>");
                       out.println("</tr>");
                        
                    }
                }
              %>
            </tbody>
          </table> 
    </body>
</html>
