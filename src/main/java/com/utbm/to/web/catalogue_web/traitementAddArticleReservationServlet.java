/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utbm.to.web.catalogue_web;

import com.utbm.to.admin.core.catalogue.core.dao.ArticleDao;
import com.utbm.to.admin.core.catalogue.core.dao.LigneReservationDao;
import com.utbm.to.admin.core.catalogue.core.dao.ReservationDao;
import com.utbm.to.admin.core.catalogue.core.entity.Article;
import com.utbm.to.admin.core.catalogue.core.entity.LigneReservation;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 
 */
public class traitementAddArticleReservationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequesUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String add = request.getParameter("add");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        System.out.println("Dbut !" + add);
         // V�rifier si c'est une requ�te d'Ajout       
        if (add.equalsIgnoreCase("add")) {     
            String client = request.getParameter("client");
            String reservation = request.getParameter("reservation");
            // Controler si les champs sont vides
            if ((!request.getParameter("quantite").equalsIgnoreCase("") && request.getParameter("quantite") != null)) {
                String quantite = request.getParameter("quantite");
                String article = request.getParameter("article");
                // Cr�ation de l'objet qui sera ins�r�
                LigneReservation ligneReservation = new LigneReservation();
                // R�cuperation de l'objet qui sera utilis� comme cl� secondaire dans la table LigneReservation
                Article articleObject = ArticleDao.readerArticle(Long.parseLong(article));
                if (articleObject.getQuantite() >= Integer.parseInt(quantite)) {
                    ligneReservation.setArticle(articleObject);
                    ligneReservation.setReservation(ReservationDao.readerReservation(Long.parseLong(reservation)));
                    ligneReservation.setQuantiteReservee(Integer.parseInt(quantite));
                    LigneReservationDao.registerLigneReservation(ligneReservation);
                    articleObject.setQuantite(articleObject.getQuantite() - Integer.parseInt(quantite));
                    // Appel de la classe ArticleDao du module core pour enregistrer l'objet
                    ArticleDao.updateArticle(articleObject);
                    out.println("Enregistrer avec succ�s !");
                    RequestDispatcher dispatcher;
                    // Rediriger vers cette page
                    dispatcher = request.getRequestDispatcher("form_reservation_addarticle.jsp?code=" + client + "&idreservation=" + reservation + "&save=success");
                    dispatcher.forward(request, response);
                } else {
                    out.println("Veuillez bien saisir les information � Enregistrer !");
                    RequestDispatcher dispatcher;
                    dispatcher = request.getRequestDispatcher("form_reservation_addarticle.jsp?code=" + client + "&idreservation=" + reservation + "&save=failed");
                    dispatcher.forward(request, response);
                }

            } else {
                out.println("Veuillez bien saisir les information � Enregistrer !");
                RequestDispatcher dispatcher;
                dispatcher = request.getRequestDispatcher("form_reservation_addarticle.jsp?code=" + client + "&idreservation=" + reservation + "&save=failed");
                dispatcher.forward(request, response);
            }

        }

    }

    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequesUpdate(request, response);
    }

   

}
