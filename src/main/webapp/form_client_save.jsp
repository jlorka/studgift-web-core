<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CategorieDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Categorie"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%
           String success="";
           success=request.getParameter("save");
           %>

           <%@ include file="menu.html" %>
        <h1>Gestions des Clients</h1>
        
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Formulaire de saisie des Clients</h3>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" action="${pageContext.request.contextPath}/traitementclient" method="POST">
               <fieldset>
                 <legend>Legend</legend>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Code :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="code" name="code" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Nom :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="nom" name="nom" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Prenom :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="prenom" name="prenom" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Rue :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="rue" name="rue" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Ville :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="ville" name="ville" placeholder="">
                   </div>
                 </div>
                   <div class="form-group">
                     <label for="codepostal" class="col-lg-2 control-label">Code Poste :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="cmdp" name="codepostal" placeholder="">
                   </div>
                 </div>
                 
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">date de Naissance :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="datepicker" name="datenais" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Email :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="email" name="email" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">T&eacute;l&eacute;phone :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="telephone" name="telephone" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Mot de passe :</label>
                   <div class="col-lg-10">
                     <input type="password" class="form-control" id="mdp" name="mdp" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Confirmer Mot de passe :</label>
                   <div class="col-lg-10">
                     <input type="password" class="form-control" id="cmdp" name="cmdp" placeholder="">
                   </div>
                 </div>
                                  
               
                 
                 
                      
                 
               
                   <%
                         if(success!=null){    
                            if(success.equalsIgnoreCase("success")){
                                out.println("<p style='color: #9C6' >Enregistrement avec succès !</p>");
                             } else if(success.equalsIgnoreCase("failed")){
                                out.println("<p style='color: #FF0000' >Erreur lors de l'enregistrement !</p>");
                             }
                         }
                        %>
                
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                       <button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>
                       <button name="add" value="add" type="submit" class="btn btn-primary">Enregistrer</button>
                   </div>
                 </div>
               </fieldset>
             </form>
            
  
</div>
</div>
  </body>
</html>
