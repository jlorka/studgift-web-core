<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%
           String success="";
           success=request.getParameter("save");
           %>

           <%@ include file="menu.html" %>
        <h1>Gestions des articles - Catalogue</h1>
        
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
      <h3 class="panel-title">Formulaire des Catalogues</h3>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" action="${pageContext.request.contextPath}/traitementcatalogue" method="POST">
               <fieldset>
                 <legend>Legend</legend>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Libell&eacute;</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="reference" name="libelle" placeholder="">
                   </div>
                 </div>
                 
                 <!--<div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Date de Cr&eacute;ation :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="marque" name="datecreation" placeholder="">
                   </div>
                 </div> -->
                 
                   <%
                         if(success!=null){    
                            if(success.equalsIgnoreCase("success")){
                                out.println("<p style='color: #9C6' >Enregistrement avec succès !</p>");
                             } else if(success.equalsIgnoreCase("failed")){
                                out.println("<p style='color: #FF0000' >Erreur lors de l'enregistrement !</p>");
                             }
                         }
                        %>
                
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                       <button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>
                       <button name="add" value="add" type="submit" class="btn btn-primary">Enregistrer</button>
                   </div>
                 </div>
               </fieldset>
             </form>
            
  <br/>
   <br/>
   <h2>Listes des Catalogues</h2>
        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>No Catalogue</th>
                <th>Libelle</th>
                <th>Date Cr&eacute;ation</th>
                <th>Modifier</th>
                <th>Supprimer</th>
              </tr>
            </thead>
            <tbody>
                
              <%
                  
                  for(Catalogue a:CatalogueDao.getAllCatalogue()){
                  out.println("<tr>");
                                    out.println("<td>"+a.getCodeCatalogue()+"</td>");
                                    out.println("<td>"+a.getLibelleCatalogue()+"</td>");
                                    out.println("<td>"+a.getDateCreation()+"</td>");
                                    out.println("<td><a href='form_catalogue_update.jsp?id="+a.getCodeCatalogue()+"'>U</a></td>");
                                    out.println("<td><a href='delete.jsp?id="+a.getCodeCatalogue()+"'>D</a></td>");
                   out.println("</tr>");
                  }
              %>
               </tbody>
        </table>
</div>
</div>
  </body>
</html>
