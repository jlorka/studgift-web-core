<%@page import="com.utbm.to.admin.core.catalogue.core.entity.*"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.4/dist/css/bootstrap.min.css">

        <!-- Optional theme -->
        <!--<link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.4/dist/css/bootstrap-theme.min.css">-->

        <!-- Latest compiled and minified JavaScript -->
        <!--<script src="${pageContext.request.contextPath}/bootstrap-3.3.4/dist/js/bootstrap.min.js"></script>-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Catalogue - Back-Office et Administration - </title>
    </head>
    <body>
    
   <h1>Authentification utilisateur</h1>
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
    <div class="panel-heading">
     <h3 class="panel-title">Connexion</h3>
     </div>
        <div class="panel-body">
        <form class="form-horizontal" action="${pageContext.request.contextPath}/connexion" method="POST">
        
        
                    <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Login :</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="login" name="login">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Mot de passe :</label>
                   <div class="col-lg-10">
                        <input type="password"  id="motdepasse" class="form-control" name="motdepasse"/>
                   </div>
                 </div>
                <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                       <button name="add" value="add" type="submit" class="btn btn-primary">Connexion</button>
                   </div>
                 </div>
        </fieldset>
             </form>
            
  
</div>
</div>
        
    </body>
</html>
