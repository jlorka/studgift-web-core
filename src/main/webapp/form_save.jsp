<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CategorieDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Categorie"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%
           String success="";
           success=request.getParameter("save");
           %>

           <%@ include file="menu.html" %>
        <h1>Gestions des articles</h1>
        
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Formulaire de saisie</h3>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" action="${pageContext.request.contextPath}/traitement" method="POST">
               <fieldset>
                 <legend>Legend</legend>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">R&eacute;f&eacute;rence</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="reference" name="reference" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">D&eacute;signation</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="designation" name="designation" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Marque</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="marque" name="marque" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Quantit&eacute;s</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="quantite" name="quantite" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Prix</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="prix" name="prix" placeholder="">
                   </div>
                 </div>
                 <div class="form-group">
                     <label for="textArea" class="col-lg-2 control-label">D&eacute;tail</label>
                   <div class="col-lg-10">
                       <textarea name="detail" class="form-control" rows="3" id="detail"></textarea>
                     <span class="help-block">Au moins 20 caractères.</span>
                   </div>
                 </div>
                  <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Adresse</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="adresse" name="adresse" placeholder="">
                   </div>
                 </div>
                 
                      
                 
                 <div class="form-group">
                   <label for="select" class="col-lg-2 control-label">Catalogue</label>
                   <div class="col-lg-10">
                     <br>
                     <select name="catalogue"  class="form-control">
                         <%
                             for(Catalogue c:CatalogueDao.getAllCatalogue()){
                                out.println("<option value='"+c.getCodeCatalogue()+"' >"+c.getLibelleCatalogue()+"</option>");
                             }
                       %>
                     </select>
                   </div>
                 </div>
                 
                 <div class="form-group">
                     <label for="select" class="col-lg-2 control-label">Cat&eacute;gorie</label>
                   <div class="col-lg-10">
                     <br>
                     <select name="categorie"  class="form-control">
                         <%
                             for(Categorie ca:CategorieDao.getAllCategorie()){
                                out.println("<option value='"+ca.getCodeCategorie()+"' >"+ca.getLibelleCategorie()+"</option>");
                             }
                       %>
                     </select>
                   </div>
                 </div>
                  <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Code Postal</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="codepostal" name="codepostal" placeholder="">
                   </div>
                 </div>
               <div class="form-group">
                     <label for="inputEmail" class="col-lg-2 control-label">Ville</label>
                   <div class="col-lg-10">
                     <input type="text" class="form-control" id="ville" name="ville" placeholder="">
                   </div>
                 </div>
                   <%
                         if(success!=null){    
                            if(success.equalsIgnoreCase("success")){
                                out.println("<p style='color: #9C6' >Enregistrement avec succès !</p>");
                             } else if(success.equalsIgnoreCase("failed")){
                                out.println("<p style='color: #FF0000' >Erreur lors de l'enregistrement !</p>");
                             }
                         }
                        %>
                
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                       <button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>
                       <button name="add" value="add" type="submit" class="btn btn-primary">Enregistrer</button>
                   </div>
                 </div>
               </fieldset>
             </form>
            
  
</div>
</div>
  </body>
</html>
