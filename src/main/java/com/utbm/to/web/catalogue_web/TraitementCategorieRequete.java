/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utbm.to.web.catalogue_web;

import com.utbm.to.admin.core.catalogue.core.dao.ArticleDao;
import com.utbm.to.admin.core.catalogue.core.dao.CategorieDao;
import com.utbm.to.admin.core.catalogue.core.dao.LignePromotionDao;
import com.utbm.to.admin.core.catalogue.core.dao.PromotionDao;
import com.utbm.to.admin.core.catalogue.core.entity.Categorie;
import com.utbm.to.admin.core.catalogue.core.entity.LignePromotion;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 
 */
public class TraitementCategorieRequete extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   
     protected void processRequesUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String add=request.getParameter("add");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
         // V�rifier si c'est une requ�te d'Ajout  
        if(add.equalsIgnoreCase("add")){
            // Controler si les champs sont vides
            if((!request.getParameter("libelle").equalsIgnoreCase("") && request.getParameter("libelle")!=null)){
                String libelle=request.getParameter("libelle");
                // Cr�ation de l'objet qui sera ins�r�
                Categorie categorie=new Categorie(null, libelle);
                // Appel de la classe CategorieDao du module core pour enregistrer l'objet
                CategorieDao.registerCategorie(categorie);
                out.println("Enregistrer avec succ�s !");
                  RequestDispatcher dispatcher;
                  // Rediriger vers cette page
                dispatcher=request.getRequestDispatcher("form_categorie_save.jsp?save=success");
                dispatcher.forward(request, response);
            }else {
                out.println("Veuillez bien saisir les information � Enregistrer !");
                 RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_categorie_save.jsp?save=failed");
                dispatcher.forward(request, response);
            }
            // V�rifier si c'est une requ�te de modification
        }  else if(add.equalsIgnoreCase("update")){
            // Controler si les champs sont vides
            if((!request.getParameter("libelle").equalsIgnoreCase("") && request.getParameter("libelle")!=null)){
                String libelle=request.getParameter("libelle");
                String code=request.getParameter("code");
                // Enregistrer la modification
                 Categorie categorie=CategorieDao.readerCategorie(Long.parseLong(code));
                 categorie.setLibelleCategorie(libelle);
                 CategorieDao.registerCategorie(categorie);
                out.println("Modification avec succ�s !");
                RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_categorie_save.jsp");
                dispatcher.forward(request, response);
                      
            } else {
                out.println("Veuillez bien saisir les information � Update !");
                RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_categorie_save.jsp");
                dispatcher.forward(request, response);
            }

        }
        
        
                 
    }
    
    
   

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequesUpdate(request, response);
    }

   

}
