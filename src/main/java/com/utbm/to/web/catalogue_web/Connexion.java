/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utbm.to.web.catalogue_web;


import com.utbm.to.web.catalogue_web.service.Operations;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


// Servlet de Traitement de la connexion
public class Connexion extends HttpServlet {
    public static final String ATT_USER         = "utilisateur";
    public static final String ATT_FORM         = "form";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    public static final String VUE              = "/index.jsp";
    public static final String login              = "/connexion.jsp";


    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page de connexion */
        this.doPost(request, response);
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        
        /* RÈcupÈration de la session depuis la requÍte */
        HttpSession session = request.getSession();
        PrintWriter out =response.getWriter();
    	response.setContentType("application/json");
        /**
         * Si aucune erreur de validation n'a eu lieu,n
         * on crÈe une session pour l'utilisateur, sinon suppression de la session.
         */
        
        if (Operations.login(request.getParameter("login"),request.getParameter("motdepasse"))) {
            session.setAttribute( ATT_SESSION_USER, "admin" );
            System.out.println(" Entrer");
            out.println("OK");
        } else {
            session.setAttribute( ATT_SESSION_USER, null );
            out.println("ERROR");
        }
        if(!request.getParameter("target").equalsIgnoreCase("ANDROID")){
            if (Operations.login(request.getParameter("login"),request.getParameter("motdepasse"))) {
        		request.setAttribute( ATT_FORM, "" );
        		request.setAttribute( ATT_USER, "Administrateur" );
        		/* Envoie de l'utilisateur sur   */
        		
        			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        		
        	}else {
        			this.getServletContext().getRequestDispatcher( login ).forward( request, response );
        		}
        }
        //     this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}