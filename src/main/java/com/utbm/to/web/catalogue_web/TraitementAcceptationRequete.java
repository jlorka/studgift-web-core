package com.utbm.to.web.catalogue_web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utbm.to.admin.core.catalogue.core.dao.AcceptationDao;
import com.utbm.to.admin.core.catalogue.core.dao.ArticleDao;
import com.utbm.to.admin.core.catalogue.core.dao.ClientDao;
import com.utbm.to.admin.core.catalogue.core.dao.ReservationDao;
import com.utbm.to.admin.core.catalogue.core.entity.Acceptation;
import com.utbm.to.admin.core.catalogue.core.entity.Reservation;

/**
 * Servlet implementation class TraitementAcceptationRequete
 */
public class TraitementAcceptationRequete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TraitementAcceptationRequete() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
    protected void processRequesUpdate(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
    	String acceptation=request.getParameter("acceptation");
    	response.setContentType("text/html;charset=UTF-8");
    	PrintWriter out = response.getWriter();
    	// V�rifier si c'est une requ�te de reservation      
    	if(acceptation.equalsIgnoreCase("acceptation")){
    		// Controler si les champs sont vides
    		if((!request.getParameter("client").equalsIgnoreCase("") && request.getParameter("client")!=null 
    				&& !request.getParameter("article").equalsIgnoreCase("") && request.getParameter("article")!=null)){
    			String client=request.getParameter("client");
    			String article=request.getParameter("article");
    			// Cr�ation de l'objet qui sera ins�r�
    			Acceptation acceptationClient=new Acceptation();
    			acceptationClient.setClient(ClientDao.readerClient(Long.valueOf(client)));
    			acceptationClient.setArticle(ArticleDao.readerArticle(Long.valueOf(article)));
    			acceptationClient.setDateAcceptation(new Date());
    			// Appel de la classe ReservationDao du module core pour enregistrer l'objet
    			AcceptationDao.registerAcceptation(acceptationClient);
    			// Rediriger vers cette page
    			out.println("OK");
    			/*RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_reservation_addarticle.jsp?code="+client+"&idreservation="+reservationClient.getCodeReservation());
                dispatcher.forward(request, response);*/
    		}else {
    			out.println("ERROR");
    			/* RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_save.jsp?save=failed");
                dispatcher.forward(request, response);*/
    		}

    	}  


    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequesUpdate(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequesUpdate(request,response);
	}

}
