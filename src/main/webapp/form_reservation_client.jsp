<%@page import="com.utbm.to.admin.core.catalogue.core.dao.ClientDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Client"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CategorieDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Categorie"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao"%>
<%@page import="com.utbm.to.admin.core.catalogue.core.entity.Catalogue"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

        <%
           String success="";
           success=request.getParameter("save");
           %>

           <%@ include file="menu.html" %>
           <h1>R&eacute;servations</h1>
        
        <br/>
        <br/><br/>
        <div class="panel panel-primary">
  <div class="panel-heading">
      <h3 class="panel-title">R&eacute;servation d'Articles</h3>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" action="${pageContext.request.contextPath}/traitementreservation" method="POST">
               <fieldset>
                 <legend>Legend</legend>
                 
                 
                 <div class="form-group">
                     <label for="select" class="col-lg-2 control-label">Choisir un Client :</label>
                   <div class="col-lg-10">
                     <br>
                     <select name="client"  class="form-control">
                         <%
                             for(Client cl:ClientDao.getAllClient()){
                                out.println("<option value='"+cl.getCodeClient()+"' >"+cl.getPrenomClient()+" "+cl.getNomClient()+" ("+cl.getCodeClient()+")</option>");
                             }
                       %>
                     </select>
                   </div>
                 </div>
                 
                
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                       <!--<button name="cancel" value="cancel" type="reset" class="btn btn-default">Annuler</button>-->
                       <button name="reservation" value="reservation" type="submit" class="btn btn-primary">R&eacute;servation</button>
                   </div>
                 </div>
               </fieldset>
             </form>
            
  
</div>
</div>
  </body>
</html>
