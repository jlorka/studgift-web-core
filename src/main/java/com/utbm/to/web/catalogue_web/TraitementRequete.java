/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utbm.to.web.catalogue_web;

import com.utbm.to.admin.core.catalogue.core.dao.ArticleDao;
import com.utbm.to.admin.core.catalogue.core.dao.CatalogueDao;
import com.utbm.to.admin.core.catalogue.core.dao.CategorieDao;
import com.utbm.to.admin.core.catalogue.core.dao.ClientDao;
import com.utbm.to.admin.core.catalogue.core.entity.Article;
import com.utbm.to.web.catalogue_web.sender.Computation;
import com.utbm.to.web.catalogue_web.utils.Utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialException;

/**
 *
 * 
 */
public class TraitementRequete extends HttpServlet {

    /**
     * Processes requests for both HTTP and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   
    
    protected void processRequesUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String add=request.getParameter("add");
        response.setContentType("application/json");
        //Formatage
    	response.setCharacterEncoding("UTF-8");        PrintWriter out = response.getWriter();
         // V�rifier si c'est une requ�te d'Ajout  
        if(request.getParameter("target").equalsIgnoreCase("ANDROID")){
        	if(add.equalsIgnoreCase("add")){
                // Controler si les champs sont vides
                if((request.getParameter("designation")!=null && !request.getParameter("designation").equalsIgnoreCase(""))
                        && (request.getParameter("marque")!=null && !request.getParameter("marque").equalsIgnoreCase("") )){
                    String designation=request.getParameter("designation");
                    String marque=request.getParameter("marque");
                    String quantite=request.getParameter("quantite");
                    String prix=request.getParameter("prix");
                    String detail=request.getParameter("detail");
                    String adresse=request.getParameter("adresse");
                    String catalogue=request.getParameter("catalogue");
                    String categorie=request.getParameter("categorie");
                    String client=request.getParameter("client");
                    String codepostal=request.getParameter("codepostal");
                    String ville=request.getParameter("ville");
                    String image=request.getParameter("image");
                    String email=request.getParameter("email");

                    Blob blob =null;
                    if(image!=null){
                    	try {
    						blob = new javax.sql.rowset.serial.SerialBlob(Utils.Base64ToBytes(image));
    					} catch (SerialException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					} catch (SQLException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}
                    }
                    Article a=new Article(null, designation, prix!=null?Float.parseFloat(prix):null,quantite!=null?Long.valueOf(quantite):null, detail, marque, blob, adresse, codepostal, ville, null, null);
                    if(catalogue!=null) a.setCatalogue(CatalogueDao.readerCatalogue(Long.parseLong(catalogue)));
                    if(categorie!=null) a.setCategorie(CategorieDao.readerCategorie(Long.parseLong(categorie)));
                    if(email!=null) a.setClient(Computation.getClientByEmail(email));
                    
                    // Appel de la classe LignePromotionDao du module core pour enregistrer l'objet
                    ArticleDao.registerArticle(a);
                    out.println("OK");
                     /* RequestDispatcher dispatcher;
                      // Rediriger vers cette page
                    dispatcher=request.getRequestDispatcher("form_save.jsp?save=success");
                    dispatcher.forward(request, response);*/
                    
                }else {
                    out.println("ERROR");
                     
                }
                // V�rifier si c'est une requ�te de modification
            }  else if(add.equalsIgnoreCase("update")){
                // Controler si les champs sont vides
                if((!request.getParameter("reference").equalsIgnoreCase("") && request.getParameter("reference")!=null)
                        && (!request.getParameter("designation").equalsIgnoreCase("") && request.getParameter("designation")!=null)
                        && (!request.getParameter("marque").equalsIgnoreCase("") && request.getParameter("marque")!=null)){
                    String reference=request.getParameter("reference");
                    String designation=request.getParameter("designation");
                    String marque=request.getParameter("marque");
                    String quantite=request.getParameter("quantite");
                    String prix=request.getParameter("prix");
                    String detail=request.getParameter("detail");
                    String adresse=request.getParameter("adresse");
                    String catalogue=request.getParameter("catalogue");
                    String categorie=request.getParameter("categorie");
                    String client=request.getParameter("client");
                    String codepostal=request.getParameter("codepostal");
                    String ville=request.getParameter("ville");
                    String email=request.getParameter("email");
                    String image=request.getParameter("image");
                    Blob blob =null;
                    if(image!=null){
                    	try {
    						blob = new javax.sql.rowset.serial.SerialBlob(Utils.Base64ToBytes(image));
    					} catch (SerialException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					} catch (SQLException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}
                    }
                    Article a=new Article(null, designation, prix!=null?Float.parseFloat(prix):null,quantite!=null?Long.parseLong(quantite):null, detail, marque, blob, adresse, codepostal, ville, null, null);
                    
                    // Cr�ation de l'objet qui sera modifi�
                    a.setCatalogue(CatalogueDao.readerCatalogue(Long.parseLong(catalogue)));
                    a.setCategorie(CategorieDao.readerCategorie(Long.parseLong(categorie)));
                    if(email!=null) a.setClient(Computation.getClientByEmail(email));
                    // Enregistrer la modification
                    ArticleDao.updateArticle(a);
                    out.println("UPDATEOK!");
                   
                          
                } else {
                    out.println("UPDATEERROR");
                }

            }
        }
        else {
        if(add.equalsIgnoreCase("add")){
            // Controler si les champs sont vides
            if((!request.getParameter("designation").equalsIgnoreCase("") && request.getParameter("designation")!=null)
                    && (!request.getParameter("marque").equalsIgnoreCase("") && request.getParameter("marque")!=null)){
                String designation=request.getParameter("designation");
                String marque=request.getParameter("marque");
                String quantite=request.getParameter("quantite");
                String prix=request.getParameter("prix");
                String detail=request.getParameter("detail");
                String adresse=request.getParameter("adresse");
                String catalogue=request.getParameter("catalogue");
                String categorie=request.getParameter("categorie");
                String client=request.getParameter("client");
                String codepostal=request.getParameter("codepostal");
                String ville=request.getParameter("ville");
                String email=request.getParameter("email");
                String image=request.getParameter("image");
                Blob blob =null;
                if(image!=null){
                	try {
						blob = new javax.sql.rowset.serial.SerialBlob(Utils.Base64ToBytes(image));
					} catch (SerialException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
                Article a=new Article(null, designation, prix!=null?Float.parseFloat(prix):null,quantite!=null?Long.parseLong(quantite):null, detail, marque, blob, adresse, codepostal, ville, null, null);
                a.setCatalogue(CatalogueDao.readerCatalogue(Long.parseLong(catalogue)));
                a.setCategorie(CategorieDao.readerCategorie(Long.parseLong(categorie)));
                //if(client!=null) a.setClient(ClientDao.readerClient(Long.parseLong(client)));
                if(email!=null) a.setClient(Computation.getClientByEmail(email));
                // Appel de la classe LignePromotionDao du module core pour enregistrer l'objet
                ArticleDao.registerArticle(a);
                out.println("Enregistrer avec succ�s !");
                  RequestDispatcher dispatcher;
                  // Rediriger vers cette page
                dispatcher=request.getRequestDispatcher("form_save.jsp?save=success");
                dispatcher.forward(request, response);
                
            }else {
                out.println("Veuillez bien saisir les information � Enregistrer !");
                 RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("form_save.jsp?save=failed");
                dispatcher.forward(request, response);
            }
            // V�rifier si c'est une requ�te de modification
        }  else if(add.equalsIgnoreCase("update")){
            // Controler si les champs sont vides
            if((!request.getParameter("reference").equalsIgnoreCase("") && request.getParameter("reference")!=null)
                    && (!request.getParameter("designation").equalsIgnoreCase("") && request.getParameter("designation")!=null)
                    && (!request.getParameter("marque").equalsIgnoreCase("") && request.getParameter("marque")!=null)){
                String reference=request.getParameter("reference");
                String designation=request.getParameter("designation");
                String marque=request.getParameter("marque");
                String quantite=request.getParameter("quantite");
                String prix=request.getParameter("prix");
                String detail=request.getParameter("detail");
                String adresse=request.getParameter("adresse");
                String catalogue=request.getParameter("catalogue");
                String categorie=request.getParameter("categorie");
                String client=request.getParameter("client");
                String codepostal=request.getParameter("codepostal");
                String ville=request.getParameter("ville");
                String image=request.getParameter("image");
                String email=request.getParameter("email");
                Blob blob =null;
                if(image!=null){
                	try {
						blob = new javax.sql.rowset.serial.SerialBlob(Utils.Base64ToBytes(image));
					} catch (SerialException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
                
                // Cr�ation de l'objet qui sera modifi�
                Article a=new Article(null, designation, prix!=null?Float.parseFloat(prix):null,quantite!=null?Long.parseLong(quantite):null, detail, marque, blob, adresse, codepostal, ville, null, null);
                a.setCatalogue(CatalogueDao.readerCatalogue(Long.parseLong(catalogue)));
                a.setCategorie(CategorieDao.readerCategorie(Long.parseLong(categorie)));
               
                if(email!=null) a.setClient(Computation.getClientByEmail(email));
                // Enregistrer la modification
                ArticleDao.updateArticle(a);
                out.println("Modification avec succ�s !");
                RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("liste_articles.jsp?update=success");
                dispatcher.forward(request, response);
                      
            } else {
                out.println("Veuillez bien saisir les information � Update !");
                RequestDispatcher dispatcher;
                dispatcher=request.getRequestDispatcher("liste_articles.jsp?update=failed");
                dispatcher.forward(request, response);
            }

        }
        }
        
        
                 
    }
    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequesUpdate(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequesUpdate(request, response);
    }


}
